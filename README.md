# SWC DB Translation

## Contributing

If you wan't to contribute, clone repo, create a branch from `main`, make your modifications (repect namings), `push`, and make a merge request.
When merge request is accepted, main will be merge on official swc-db repo and a new version will be build with your translations.

## Authors and acknowledgment

Thanks to all contributors.

## License

Free to reuse translations.
